<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/sample', function() {
    return view('sample');
});

Route::get('/sampleTwo/{id}/{token}', function($id, $token) {
    return "SAMPLE TWO ".$id." ".$token;
})->name('admin.sample');

Auth::routes();

Route::prefix('contact')->group(function() {
    Route::get('/', 'ContactController@index')->name('contact.index');
    Route::post('/store', 'ContactController@store')->name('contact.store');
});

Route::get('/home', 'PostsController@index')->name('home');
Route::prefix('post')->group(function() {
    Route::get('/create', 'PostsController@create')->name('post.create');
    Route::post('/store', 'PostsController@store')->name('post.store');
    Route::get('/show/{id}', 'PostsController@show')->name('post.show');
    Route::get('/edit/{id}', 'PostsController@edit')->name('post.edit');
    Route::put('/update/{id}', 'PostsController@update')->name('post.update');
    Route::get('/destroy/{id}', 'PostsController@destroy')->name('post.destroy');
});



//Route::resource('post', 'PostsController');
