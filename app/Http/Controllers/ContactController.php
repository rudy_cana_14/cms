<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index() {
        return view('contact.create');
    }

    public function store(Request $request) {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        Mail::to('test@test.com')->send(new ContactMail($data));

        session()->flash('success', 'Email is successfully send.');
        return redirect()->route('contact.index');
        // return redirect()->route('contact.index')->with('success', 'Email is successfully send.');
    }
}
