<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use DB;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        //$this->middleware('auth')->only(['post.create']);
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        // $posts = DB::table('posts')
        //     ->join('users', 'posts.user_id', '=', 'users.id')
        //     ->select('posts.*', 'users.name', 'users.user_image')
        //     ->orderBy('posts.id', 'DESC')
        //     ->get();
        //$posts =  Post::orderBy('title', 'asc')->paginate(10);

        $posts = Post::with('user')->orderBy('id', 'DESC')->get();
        //$posts = Post::Rudy()->get();
        //$posts = Post::Ronald()->get();
        return view('post/index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('POST')) {
            // VALIDATE FIELDS
            // $data = request()->validate([
            //     'title' => 'required',
            //     'body' => 'required',
            //     'image' => 'image|nullable|max:1999'
            // ]);

            $this->validate($request, [
                'title' => 'required|min:5',
                'body' => 'required',
                'image' => 'image|nullable|max:1999'
            ]);

            // Handle File Upload
            if($request->hasFile('image')) {
                $filenameWithExt = $request->file('image')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileNameStore = $filename.'_'.time().'.'.$extension;
                $path = $request->file('image')->storeAs('public/cover_image', $fileNameStore);
            }
            else {
                $fileNameStore = 'noimage.jpg';
            }

            // CREATE NEW POST

            Post::create([
                'title' => $request['title'],
                'body' => $request['body'],
                'image' => $fileNameStore,
                'user_id' => auth()->user()->id
            ]);

            return redirect()->route('post.create')->with('success', 'Post Created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = POST::findOrFail($id);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = POST::findOrFail($id);
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->method() == "PUT") {
            // Validate Fields
            $this->validate($request, [
                'title' => 'required',
                'body' => 'required',
                'image' => 'image|nullable|max:1999'
            ]);

            // Handle File Upload
            if($request->hasFile('image')) {
                $filenameWithExt = $request->file('image')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension();
                $fileNameStore = $filename.'_'.time().'.'.$extension;
                $path = $request->file('image')->storeAs('public/cover_image', $fileNameStore);
            }
            else {
                $fileNameStore = 'noimage.jpg';
            }

            //Update Post Record
            POST::where('id', $id)->update([
                'title' => $request['title'],
                'body' => $request['body'],
                'image' => $fileNameStore
            ]);

            return redirect()->route('post.edit', $id)->with('success', 'Post updated.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        POST::findOrFail($id)->delete();
        return redirect()->route('home');
    }
}
