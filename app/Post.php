<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // Table Name
    protected $table = 'posts';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    // OPTION 1 - ALLOW FIELD TO MASS ASSIGNMENT
    // protected $fillable = [
    //     'image', 'title', 'body', 'user_id'
    // ];

    // OPTION 2 - ALLOW ALL FIELDS TO MASS ASSIGNMENT
    protected $guarded = [];

    public function user() {
        //return $this->belongsTo('App\User');
        return $this->belongsTo(User::class);
    }

    public function scopeRudy($query) {
        return $query->where('user_id', 2)->orderBy('id', 'DESC');
    }

    public function scopeRonald($query) {
        return $query->where('user_id', 1)->orderBy('id', 'DESC');
    }
}
