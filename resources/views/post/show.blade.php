@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('include.messages')
            <div class="card">
                <div class="card-header"><i class="fa fa-info-circle"></i>
                     POST VIEW
                    <a href="{{ route('home') }}" class="btn btn-outline-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-4" >
                            <img style="width:100%; height:200px" class="img-thumbnail" src="{{ url('/storage/cover_image', [$post->image]) }}">
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <h5 class="card-title">
                                <i class="fa fa-quote-left fa-fw pull-left"></i>
                                {{ $post->title }}
                                <i class="fa fa-quote-right fa-fw"></i>
                            </h5>
                            {!! nl2br(e($post->body)) !!}
                            <br>
                            <span class="badge badge-primary p-2 mt-2">Written on {{ date('M d, Y h:m A', strtotime($post->created_at)) }}</span>

                            <span class="badge badge-primary p-2 mt-2">By: {{ $post->user->name }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('myjs/posts.js') }}"></script>
@endsection
