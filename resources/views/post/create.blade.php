@extends('layouts.app')
@section('title', 'Create post')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            @include('include.messages')
            <div class="card">
                <div class="card-header"><i class="fa fa-edit"></i> CREATE POST</div>

                <div class="card-body">
                    <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" placeholder="Title">
                            @error('title')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea class="form-control" rows="5"  id="body" name="body">{{ old('body') }}</textarea>
                            @error('body')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="custom-file">
                            <input type="file" class="form-control" id="image" name="image">
                            @error('image')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>
                        <br>
                        <br>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ asset('myjs/posts.js') }}"></script>
    <script>
        let post = new Post();
        post.create(['safgsag']);
    </script>
@endsection
