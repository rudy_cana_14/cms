@extends('layouts.app')

@section('title', 'Home')
@section('header')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/timeline.css') }}">
@endsection

@section('content')
<div class="container">
    @if(Auth::check())
        <div class="col-md-11 col-xs-12" style="margin:auto">
            <div class="panel panel-info" style="margin-left:10%">
                <div class="panel-heading"><i class="fa fa-plus-circle"></i> CREATE POST
                    <a href="{{ route('post.create') }}" class="float-right btn btn-outline-primary mb-2"><i class="fa fa-plus"></i> Create post</a>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success" role="alert">
                        Hey {{ Auth::user()->name }}, what is your mind?
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div id="content" class="content content-full-width" >


        <!-- begin profile-content -->
        <div class="profile-content" >
           <!-- begin tab-content -->
           <div class="tab-content p-0" >
              <!-- begin #profile-post tab -->
              <div class="tab-pane fade active show" id="profile-post" >
                 <!-- begin timeline -->
                 <ul class="timeline" >
                    @forelse($posts as $post)

                        <li>
                            <!-- begin timeline-time -->
                           <div class="timeline-time">
                            <span class="date">{{ date('M d, Y', strtotime($post->created_at)) }}</span>
                              <span class="time">{{ date('h:m A', strtotime($post->created_at)) }}</span>
                           </div>
                           <!-- end timeline-time -->
                           <!-- begin timeline-icon -->
                           <div class="timeline-icon">
                              <a href="javascript:;">&nbsp;</a>
                           </div>
                       <!-- end timeline-icon -->
                           <!-- begin timeline-body -->
                           <div class="timeline-body" >
                              <div class="timeline-header">
                                 <span class="userimage" style="height: 42px; width:42px; margin-top: -6px; border: 2px solid #3578E5;;"><img src="{{ url('/storage/cover_image', [$post->user->user_image]) }}" alt="" class="circle-profile" style="height: 50px; "></span>
                                <span class="username"><a href="javascript:;">{{ ucwords(strtolower($post->user->name)) }}</a> <small></small></span>

                                @if(Auth::check())
                                    @if($post->user->id == Auth::user()->id)
                                            <span class="pull-right mt-0">
                                                <div class="dropdown">
                                                    <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-bars"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="{{ route('post.edit', $post->id) }}"><i class="fa fa-edit"></i> Edit post</a>
                                                        <a class="dropdown-item" href="{{ route('post.destroy', $post->id) }}"><i class="fa fa-trash"></i> Delete post</a>
                                                    </div>
                                                </div>
                                            </span>
                                    @endif
                                @endif

                              </div>
                              <div class="timeline-content">
                                    <h4>
                                        <a href="{{ route('post.show', $post->id) }}">
                                            <i class="fa fa-quote-left fa-fw pull-left"></i>
                                            {{ $post->title }}
                                            <i class="fa fa-quote-right fa-fw"></i>
                                        </a>
                                    </h4>

                                  <div class="row">
                                    @if($post->image == 'noimage.jpg')
                                        @if(strlen($post->body) > 20)
                                            <p class="lead post-content pd-4">
                                                {!! nl2br(e($post->body)) !!}
                                            </p>
                                        @else
                                            <p class="pl-4" style="font-size:35px">
                                                {!! nl2br(e($post->body)) !!}
                                            </p>
                                        @endif
                                    @else
                                        <div class="col-xs-12 col-md-5">
                                            <img src="{{ url('/storage/cover_image', [$post->image]) }}" class="mt-1 img-thumbnail">
                                        </div>
                                        <div class="col-xs-12 col-md-7">
                                            <p class="lead post-content" >
                                            @if(strlen($post->body) > 20)
                                                <p class="lead post-content">
                                                    {!! nl2br(e($post->body)) !!}
                                                </p>
                                            @else
                                                <p class="" style="font-size:35px">
                                                    {!! nl2br(e($post->body)) !!}
                                                </p>
                                            @endif
                                            </p>
                                        </div>
                                    @endif
                                  </div>


                              </div>
                              <div class="timeline-likes">
                                 <div class="stats-right">
                                    <span class="stats-text">259 Shares</span>
                                    <span class="stats-text">21 Comments</span>
                                 </div>
                                 <div class="stats">
                                    <span class="fa-stack fa-fw stats-icon">
                                    <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                    <i class="fa fa-heart fa-stack-1x fa-inverse t-plus-1"></i>
                                    </span>
                                    <span class="fa-stack fa-fw stats-icon">
                                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                    <i class="fa fa-thumbs-up fa-stack-1x fa-inverse"></i>
                                    </span>
                                    <span class="stats-total">4.3k</span>
                                 </div>
                              </div>
                              <div class="timeline-footer">
                                 <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-thumbs-up fa-fw fa-lg m-r-3"></i> Like</a>
                                 <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-comments fa-fw fa-lg m-r-3"></i> Comment</a>
                                 <a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-share fa-fw fa-lg m-r-3"></i> Share</a>
                              </div>
                              <div class="timeline-comment-box" style="background-color:white">

                                  <p>posted text </p>
                                 <div class="user"><img src="{{ asset('images/noimage.jpg') }}"></div>
                                 <div class="input">
                                    <form action="">
                                       <div class="input-group">
                                          <input style="background-color:#f2f3f5;border-top-left-radius:0;border-bottom-left-radius:0;" type="text" class="form-control rounded-corner" placeholder="Write a comment...">
                                          <button style="border-top-left-radius:0;border-bottom-left-radius:0;" class="btn btn-primary f-s-12 rounded-corner" type="button"><span class="input-group-btn fa fa-comment"></span></button>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <!-- end timeline-body -->
                        </li>
                        @empty
                            <h4>NO RECORDS AVAILABLE</h4>

                        @endforelse
                     </ul>
                 <!-- end timeline -->
              </div>
              <!-- end #profile-post tab -->
           </div>
           <!-- end tab-content -->
        </div>
        <!-- end profile-content -->
     </div>
</div>
@endsection

@section('footer')
@endsection
