@extends('layouts.app')
@section('title', 'Contact')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><i class="fa fa-phone"></i> CONTACT US</div>

                <div class="card-body">
                    <form action="{{ route('contact.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control" placeholder="Name">
                            @error('name')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
                            @error('email')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="message">Message:</label>
                            <textarea class="form-control" rows="5"  id="message" name="message">{{ old('message') }}</textarea>
                            @error('message')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary mt-4"><i class="fa fa-send"></i> Send message</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
